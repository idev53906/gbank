package fr.jayrex.gbank.storage;

import fr.jayrex.gbank.GBank;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class YAMLStorage implements StorageInterface {
    private final File playerDataFile;
    private final FileConfiguration playerDataConfig;

    public YAMLStorage() {
        this.playerDataFile = new File(GBank.getInstance().getDataFolder(), "playerData.yml");
        this.playerDataConfig = YamlConfiguration.loadConfiguration(playerDataFile);
    }

    @Override
    public void initialize() throws Exception {
        if (!playerDataFile.exists()) {
            playerDataFile.createNewFile();
        }
    }

    @Override
    public Map<String, Double> getPlayerBalances(UUID playerId) {
        Map<String, Double> balances = new HashMap<>();
        if (playerDataConfig.contains(playerId.toString())) {
            for (String key : playerDataConfig.getConfigurationSection(playerId.toString()).getKeys(false)) {
                balances.put(key, playerDataConfig.getDouble(playerId.toString() + "." + key));
            }
        }
        return balances;
    }

    @Override
    public void setPlayerBalance(UUID playerId, String currency, double amount) {
        playerDataConfig.set(playerId.toString() + "." + currency, amount);
        saveConfig();
    }

    @Override
    public void updatePlayerBalance(UUID playerId, String currency, double change) {
        double currentBalance = getPlayerBalances(playerId).getOrDefault(currency, 0.0);
        playerDataConfig.set(playerId.toString() + "." + currency, currentBalance + change);
        saveConfig();
    }

    @Override
    public void removePlayer(UUID playerId) {
        playerDataConfig.set(playerId.toString(), null);
        saveConfig();
    }

    private void saveConfig() {
        try {
            playerDataConfig.save(playerDataFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
