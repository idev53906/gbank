package fr.jayrex.gbank.storage;

import java.util.Map;
import java.util.UUID;

public interface StorageInterface {
    void initialize() throws Exception;
    Map<String, Double> getPlayerBalances(UUID playerId);
    void setPlayerBalance(UUID playerId, String currency, double amount);
    void updatePlayerBalance(UUID playerId, String currency, double change);
    void removePlayer(UUID playerId);
}

