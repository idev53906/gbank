package fr.jayrex.gbank.storage;

import fr.jayrex.gbank.manager.DatabaseManager;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MySQLStorage implements StorageInterface {
    private final DatabaseManager databaseManager;

    public MySQLStorage(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public void initialize() throws Exception {
        Connection conn = null;
        try {
            conn = databaseManager.getConnection();
            if (conn == null || conn.isClosed()) {
                throw new SQLException("Unable to establish a database connection.");
            }
            setupDatabase(conn);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    System.err.println("Error closing the database connection.");
                    e.printStackTrace();
                }
            }
        }
    }

    private void setupDatabase(Connection conn) throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            stmt.execute("CREATE TABLE IF NOT EXISTS player_balances (" +
                    "player_id VARCHAR(36) NOT NULL," +
                    "currency VARCHAR(255) NOT NULL," +
                    "amount DOUBLE NOT NULL," +
                    "PRIMARY KEY (player_id, currency))");
        } catch (SQLException e) {
            System.err.println("Error setting up the database tables.");
            throw e;
        }
    }

    @Override
    public Map<String, Double> getPlayerBalances(UUID playerId) {
        Map<String, Double> balances = new HashMap<>();
        String query = "SELECT currency, amount FROM player_balances WHERE player_id = ?";

        try (Connection conn = databaseManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, playerId.toString());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    balances.put(rs.getString("currency"), rs.getDouble("amount"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return balances;
    }

    @Override
    public void setPlayerBalance(UUID playerId, String currency, double amount) {
        String update = "REPLACE INTO player_balances (player_id, currency, amount) VALUES (?, ?, ?)";

        try (Connection conn = databaseManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(update)) {
            ps.setString(1, playerId.toString());
            ps.setString(2, currency);
            ps.setDouble(3, amount);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void updatePlayerBalance(UUID playerId, String currency, double change) {
        String update = "UPDATE player_balances SET amount = amount + ? WHERE player_id = ? AND currency = ?";

        try (Connection conn = databaseManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(update)) {
            ps.setDouble(1, change);
            ps.setString(2, playerId.toString());
            ps.setString(3, currency);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removePlayer(UUID playerId) {
        String delete = "DELETE FROM player_balances WHERE player_id = ?";

        try (Connection conn = databaseManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(delete)) {
            ps.setString(1, playerId.toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
