package fr.jayrex.gbank.menu;

import fr.jayrex.gbank.GBank;
import fr.jayrex.gbank.manager.PlayerProfileManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.ChatColor;

import java.util.Map;
import java.util.UUID;

public class BalanceMenu extends PaginatedMenu {
    private UUID playerId;

    public BalanceMenu(Player player, UUID playerId) {
        super(player);
        this.playerId = playerId;
    }

    @Override
    public String getMenuName() {
        return GBank.getInstance().getConfigManager().getMessage("menu.title");
    }

    @Override
    public int getRows() {
        return 6;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {
        e.setCancelled(true);
        Player player = (Player) e.getWhoClicked();
        ItemStack clickedItem = e.getCurrentItem();
        PlayerProfileManager profileManager = GBank.getInstance().getPlayerProfileManager();
        if (clickedItem == null || !clickedItem.hasItemMeta()) return;

        String itemName = clickedItem.getItemMeta().getDisplayName();

        if (itemName.equals(ChatColor.GREEN + "Next Page")) {
            if (page < (int) Math.ceil((double) profileManager.getPlayerBalances(playerId).size() / maxItemsPerPage)) {
                page++;
                super.open();
            }
        } else if (itemName.equals(ChatColor.GREEN + "Previous Page")) {
            if (page > 0) {
                page--;
                super.open();
            }
        } else if (itemName.equals(ChatColor.RED + "Close Menu")) {
            player.closeInventory();
        } else {
            handleCurrencySelection(player, clickedItem);
        }
    }

    private void handleCurrencySelection(Player player, ItemStack item) {
        String displayName = item.getItemMeta().getDisplayName();
        String currency = ChatColor.stripColor(displayName.split(":")[0]);

        PlayerProfileManager profileManager = GBank.getInstance().getPlayerProfileManager();
        double amount = profileManager.getPlayerBalances(playerId).getOrDefault(currency, 0.0);

        player.sendMessage(GBank.getInstance().getConfigManager().getMessage("balance.specific").replace("{currency}", currency).replace("{amount}", String.valueOf(amount)));
    }

    @Override
    public void setMenuItems() {
        PlayerProfileManager profileManager = GBank.getInstance().getPlayerProfileManager();
        Map<String, Double> balances = profileManager.getPlayerBalances(playerId);

        balances.forEach((currency, amount) -> {
            ItemStack itemStack = makeItem(Material.GOLD_INGOT, ChatColor.GOLD + currency + ": " + amount);
            addButton(itemStack);
        });

        addMenuBorder();
    }

    private void addButton(ItemStack item) {
        inventory.addItem(item);
    }

    @Override
    public void addMenuBorder() {
        inventory.setItem(48, makeItem(Material.ARROW, ChatColor.GREEN + "Previous Page"));
        inventory.setItem(50, makeItem(Material.ARROW, ChatColor.GREEN + "Next Page"));
        inventory.setItem(49, makeItem(Material.BARRIER, ChatColor.RED + "Close Menu"));
    }
}
