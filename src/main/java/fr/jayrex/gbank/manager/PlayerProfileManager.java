package fr.jayrex.gbank.manager;

import fr.jayrex.gbank.GBank;
import fr.jayrex.gbank.storage.StorageInterface;
import fr.jayrex.gbank.storage.MySQLStorage;
import fr.jayrex.gbank.storage.YAMLStorage;

import java.util.Map;
import java.util.UUID;

public class PlayerProfileManager {
    private StorageInterface storage;

    public PlayerProfileManager() {
        try {
            initializeStorage();
        } catch (Exception e) {
            GBank.getInstance().getLogger().severe("Failed to initialize storage: " + e.getMessage());
            GBank.getInstance().getServer().getPluginManager().disablePlugin(GBank.getInstance());
        }
    }

    private void initializeStorage() throws Exception {
        String storageType = GBank.getInstance().getConfigManager().getStorageType().toUpperCase();
        if ("MYSQL".equals(storageType)) {
            this.storage = new MySQLStorage(GBank.getInstance().getDatabaseManager());
        } else if ("YAML".equals(storageType)) {
            this.storage = new YAMLStorage();
        } else {
            throw new IllegalArgumentException("Unsupported storage type: " + storageType);
        }
        this.storage.initialize();
    }

    public Map<String, Double> getPlayerBalances(UUID playerId) {
        return storage.getPlayerBalances(playerId);
    }


    public void setPlayerBalance(UUID playerId, String currency, double amount) {
        storage.setPlayerBalance(playerId, currency, amount);
    }

    public void updatePlayerBalance(UUID playerId, String currency, double change) {
        storage.updatePlayerBalance(playerId, currency, change);
    }

}
