package fr.jayrex.gbank.manager;

import org.bukkit.configuration.file.FileConfiguration;
import fr.jayrex.gbank.GBank;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ConfigManager {

    private final GBank plugin;
    private FileConfiguration config;
    private FileConfiguration languageConfig;
    private Map<String, Currency> currencies;


    public ConfigManager(GBank plugin) {
        this.plugin = plugin;
        this.config = plugin.getConfig();
        loadConfiguration();
        loadLanguageFile();
        loadCurrencies();
    }

    private void loadConfiguration() {
        config.options().copyDefaults(true);
        plugin.saveConfig();
    }

    private void loadLanguageFile() {
        File languageFile = new File(plugin.getDataFolder(), "language.yml");
        if (!languageFile.exists()) {
            plugin.saveResource("language.yml", false);
        }
        languageConfig = YamlConfiguration.loadConfiguration(languageFile);
    }

    private void loadCurrencies() {
        if (config.isConfigurationSection("currencies")) {
            currencies = new HashMap<>();
            for (String key : config.getConfigurationSection("currencies").getKeys(false)) {
                String symbol = config.getString("currencies." + key + ".symbol");
                currencies.put(key, new Currency(symbol));
            }
        } else {
            plugin.getLogger().warning("No 'currencies' section found in config.yml");
            currencies = new HashMap<>();
        }
    }

    public void reload() {
        plugin.reloadConfig();
        config = plugin.getConfig();
        loadCurrencies();
    }

    public String getMessage(String key) {
        return languageConfig.getString("messages." + key, "Message not found for key: " + key);
    }

    public String getStorageType() {
        return config.getString("storage.type", "MYSQL");
    }

    public String getDatabaseHost() {
        return config.getString("database.host");
    }

    public String getDatabaseUser() {
        return config.getString("database.user");
    }

    public String getDatabasePassword() {
        return config.getString("database.password");
    }

    public String getDatabaseName() {
        return config.getString("database.dbname");
    }

    public int getDatabasePort() {
        return config.getInt("database.port", 3306);
    }

    public int getTaxRate() {
        return config.getInt("tax.rate", 5);
    }

    public Map<String, Currency> getCurrencies() {
        return currencies;
    }

    public static class Currency {
        private final String symbol;


        public Currency(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }

    }
}
