package fr.jayrex.gbank.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
    private final ConfigManager configManager;
    private Connection connection;

    public DatabaseManager(ConfigManager configManager) {
        this.configManager = configManager;
        initDatabaseConnection();
    }

    private void initDatabaseConnection() {
        try {
            String connectionString = "jdbc:mysql://" +
                    configManager.getDatabaseHost() + ":" +
                    configManager.getDatabasePort() + "/" +
                    configManager.getDatabaseName() +
                    "?user=" + configManager.getDatabaseUser() +
                    "&password=" + configManager.getDatabasePassword() +
                    "&useSSL=false&autoReconnect=true";

            this.connection = DriverManager.getConnection(connectionString);
            System.out.println("Connected to MySQL database!");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Failed to connect to MySQL database.");
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                System.out.println("Database connection closed successfully.");
            } catch (SQLException e) {
                System.out.println("Failed to close the database connection.");
                e.printStackTrace();
            }
        }
    }
}
