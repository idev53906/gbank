package fr.jayrex.gbank;

import fr.jayrex.gbank.command.*;
import fr.jayrex.gbank.manager.*;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class GBank extends JavaPlugin {
    private static GBank instance;
    private DatabaseManager databaseManager;
    private ConfigManager configManager;
    private PlayerProfileManager playerProfileManager;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        configManager = new ConfigManager(this);
        databaseManager = new DatabaseManager(configManager);
        playerProfileManager = new PlayerProfileManager();

        registerCommands();

        getLogger().info("GBank has been enabled!");

        long period = getConfig().getLong("distribution.periods", 300) * 20;

        new CurrencyDistributor(this).runTaskTimer(this, period, period);
    }

    private void registerCommands() {
        getCommand("balance").setExecutor(new BalanceCommand());
        getCommand("bank").setExecutor(new BankCommand(playerProfileManager));
        getCommand("pay").setExecutor(new PayCommand(playerProfileManager));
        getCommand("gbankreload").setExecutor(new ReloadCommand());
    }

    @Override
    public void onDisable() {
        if (databaseManager != null) {
            databaseManager.closeConnection();
        }
        getLogger().info("GBank has been disabled.");
    }

    public static GBank getInstance() {
        return instance;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public PlayerProfileManager getPlayerProfileManager() {
        return playerProfileManager;
    }
}
