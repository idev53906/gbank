package fr.jayrex.gbank;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import fr.jayrex.gbank.manager.PlayerProfileManager;
import fr.jayrex.gbank.manager.ConfigManager.Currency;

import java.util.Map;
import java.util.UUID;

public class CurrencyDistributor extends BukkitRunnable {
    private final GBank plugin;

    public CurrencyDistributor(GBank plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        PlayerProfileManager profileManager = plugin.getPlayerProfileManager();
        Map<String, Currency> currencies = plugin.getConfigManager().getCurrencies();

        Bukkit.getOnlinePlayers().forEach(player -> {
            UUID playerId = player.getUniqueId();
            currencies.forEach((currencyCode, currency) -> {
                double amount = plugin.getConfig().getDouble("distribution.amounts." + currencyCode, 10.0);
                profileManager.updatePlayerBalance(playerId, currencyCode, amount);
                player.sendMessage(plugin.getConfigManager().getMessage("balance.update")
                        .replace("{amount}", String.format("%.2f", amount))
                        .replace("{currency}", currencyCode)
                        .replace("{symbol}", currency.getSymbol()));
            });
        });
    }
}
