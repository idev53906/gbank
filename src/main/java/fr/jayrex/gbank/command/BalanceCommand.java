package fr.jayrex.gbank.command;

import fr.jayrex.gbank.GBank;
import fr.jayrex.gbank.manager.ConfigManager;
import fr.jayrex.gbank.manager.PlayerProfileManager;
import fr.jayrex.gbank.menu.BalanceMenu;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class BalanceCommand implements CommandExecutor {
    private final PlayerProfileManager profileManager;

    public BalanceCommand() {
        this.profileManager = GBank.getInstance().getPlayerProfileManager();
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        ConfigManager configManager = GBank.getInstance().getConfigManager();
        if (!(sender instanceof Player)) {
            sender.sendMessage(configManager.getMessage("command.player_only"));
            return true;
        }

        Player player = (Player) sender;
        UUID playerId = player.getUniqueId();

        if (args.length == 0) {
            BalanceMenu menu = new BalanceMenu(player, playerId);
            menu.open();
        } else if (args.length == 1) {
            String currency = args[0];
            double balance = profileManager.getPlayerBalances(playerId).getOrDefault(currency, 0.0);
            player.sendMessage(configManager.getMessage("balance.specific").replace("{currency}", currency).replace("{amount}", String.valueOf(balance)));
        } else if (args.length == 2) {
            Player targetPlayer = Bukkit.getPlayer(args[1]);
            if (targetPlayer == null) {
                player.sendMessage(configManager.getMessage("bank.player_not_found"));
                return true;
            }
            UUID targetId = targetPlayer.getUniqueId();
            String currency = args[0];
            double balance = profileManager.getPlayerBalances(targetId).getOrDefault(currency, 0.0);
            player.sendMessage(configManager.getMessage("balance.specific").replace("{player}", targetPlayer.getName()).replace("{currency}", currency).replace("{amount}", String.valueOf(balance)));
        } else {
            player.sendMessage(configManager.getMessage("balance.usage"));
        }
        return true;
    }
}
