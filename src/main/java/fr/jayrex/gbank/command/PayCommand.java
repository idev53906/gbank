package fr.jayrex.gbank.command;

import fr.jayrex.gbank.GBank;
import fr.jayrex.gbank.manager.ConfigManager;
import fr.jayrex.gbank.manager.PlayerProfileManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class PayCommand implements CommandExecutor {

    private final PlayerProfileManager profileManager;

    public PayCommand(PlayerProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        ConfigManager configManager = GBank.getInstance().getConfigManager();
        if (!(sender instanceof Player)) {
            sender.sendMessage(configManager.getMessage("command.player_only"));
            return false;
        }

        Player player = (Player) sender;
        if (args.length < 3) {
            player.sendMessage(configManager.getMessage("pay.usage"));
            return false;
        }

        Player recipient = GBank.getInstance().getServer().getPlayer(args[0]);
        if (recipient == null) {
            player.sendMessage(configManager.getMessage("pay.player_not_found"));
            return false;
        }

        String currency = args[1];
        if (!configManager.getCurrencies().containsKey(currency)) {
            player.sendMessage(configManager.getMessage("pay.invalid_currency"));
            return false;
        }

        double amount = Double.parseDouble(args[2]);
        if (amount <= 0) {
            player.sendMessage(configManager.getMessage("pay.amount_positive"));
            return false;
        }

        UUID payerId = player.getUniqueId();
        UUID recipientId = recipient.getUniqueId();

        double taxRate = configManager.getTaxRate() / 100.0;
        double taxAmount = amount * taxRate;
        double netAmount = amount - taxAmount;

        if (profileManager.getPlayerBalances(payerId).getOrDefault(currency, 0.0) < amount) {
            player.sendMessage(configManager.getMessage("pay.not_enough_balance"));
            return false;
        }

        profileManager.updatePlayerBalance(payerId, currency, -amount);
        profileManager.updatePlayerBalance(recipientId, currency, netAmount);

        player.sendMessage(configManager.getMessage("pay.success").replace("{recipient}", recipient.getName()).replace("{amount}", String.format("%.2f", netAmount)).replace("{currency}", currency).replace("{symbol}", configManager.getCurrencies().get(currency).getSymbol()).replace("{tax}", String.format("%.2f", taxAmount)));
        recipient.sendMessage(configManager.getMessage("pay.recipient_notify").replace("{amount}", String.format("%.2f", netAmount)).replace("{currency}", currency).replace("{symbol}", configManager.getCurrencies().get(currency).getSymbol()).replace("{sender}", player.getName()));
        return true;
    }
}


