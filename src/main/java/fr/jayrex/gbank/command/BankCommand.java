package fr.jayrex.gbank.command;

import fr.jayrex.gbank.GBank;
import fr.jayrex.gbank.manager.ConfigManager;
import fr.jayrex.gbank.manager.PlayerProfileManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class BankCommand implements CommandExecutor {

    private final PlayerProfileManager profileManager;

    public BankCommand(PlayerProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        ConfigManager configManager = GBank.getInstance().getConfigManager();
        if (!(sender instanceof Player)) {
            sender.sendMessage(configManager.getMessage("command.player_only"));
            return false;
        }

        Player player = (Player) sender;

        if (args.length < 4) {
            player.sendMessage(configManager.getMessage("bank.usage"));
            return false;
        }

        String action = args[0].toLowerCase();
        Player targetPlayer = GBank.getInstance().getServer().getPlayer(args[1]);
        if (targetPlayer == null) {
            player.sendMessage(configManager.getMessage("bank.player_not_found"));
            return false;
        }

        String currency = args[2];
        if (!configManager.getCurrencies().containsKey(currency)) {
            player.sendMessage(configManager.getMessage("bank.invalid_currency"));
            return false;
        }

        double amount = Double.parseDouble(args[3]);
        UUID targetUuid = targetPlayer.getUniqueId();

        switch (action) {
            case "give":
                profileManager.updatePlayerBalance(targetUuid, currency, amount);
                break;
            case "set":
                profileManager.setPlayerBalance(targetUuid, currency, amount);
                break;
            case "take":
                profileManager.updatePlayerBalance(targetUuid, currency, -amount);
                break;
            default:
                player.sendMessage(configManager.getMessage("bank.invalid_action"));
                return false;
        }

        player.sendMessage(configManager.getMessage("bank.success").replace("{action}", action).replace("{amount}", String.valueOf(amount)).replace("{symbol}", configManager.getCurrencies().get(currency).getSymbol()));
        return true;
    }
}
