package fr.jayrex.gbank.command;

import fr.jayrex.gbank.GBank;
import fr.jayrex.gbank.manager.ConfigManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class ReloadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        GBank plugin = GBank.getInstance();
        ConfigManager configManager = GBank.getInstance().getConfigManager();
        plugin.reloadConfig();
        plugin.getConfigManager().reload();
        sender.sendMessage(configManager.getMessage("reload.success"));
        return true;
    }
}
